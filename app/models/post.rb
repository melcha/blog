class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates :body, presence: true
  validates :title, presence: true
end
